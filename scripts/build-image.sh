
#!/bin/bash

# login with needed permissions
echo $GCP_IMAGE_BUILDER > gcloud-service-key.json
gcloud auth activate-service-account --key-file gcloud-service-key.json
gcloud config set project $GCP_PROJECT

# generate config and build the image
# cat deploy/build.yaml | envsubst > build.yaml
# gcloud builds submit --config build.yaml

# build and push the image
gcloud builds submit --gcs-log-dir gs://$BUILD_LOGS_BUCKET -t gcr.io/$GCP_PROJECT/group:$IMAGE_VERSION .

