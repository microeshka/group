import { status, ServiceError, Metadata } from 'grpc';

export const checkAndGetEnv = (envName: string, errMsg?: string): string => {
  const envVar = process.env[envName];
  if (envVar === undefined) {
    const error = new Error(errMsg || `${envName} not found in environment variables`);
    console.error(error);
    throw error;
  }
  return envVar;
};

export const parseCookies = (cookie: string | undefined): Map<string, string> => {
  const res = new Map<string, string>();
  console.log({ cookie });
  if (!cookie) {
    return res;
  }
  for (const c of cookie.split(';')) {
    const [name, value] = c.split('=');
    res.set(name, value);
  }
  return res;
};

export const statusCodetoGrpcStatus = (code: number) => {
  switch (code) {
    case 404: return status.NOT_FOUND;
    case 400: return status.INVALID_ARGUMENT;
    case 500: return status.INTERNAL;
    case 401: return status.UNAUTHENTICATED;
    case 403: return status.PERMISSION_DENIED;
    default: return status.UNKNOWN;
  }
};

export const errorToGrpcError = (err: any): ServiceError => {
  const httpStatus = err.status || 500;
  const metadata = new Metadata();
  metadata.add('status', httpStatus + '');
  return { code: statusCodetoGrpcStatus(httpStatus), details: err.errorMessage || 'Internal server error', metadata } as ServiceError;
};
