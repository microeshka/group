
export interface LoggedUser {
    username: string;
    fullName: string;
    userId: string;
}

const users = new Map<string, LoggedUser>();

export const addUser = (key: string, value: LoggedUser) => {
  users.set(key, value);
};

export const deleteUser = (key: string) => {
  users.delete(key);
};

export const getUser = (key: string): LoggedUser | undefined => {
  const user = users.get(key);
  deleteUser(key);
  return user;
};
