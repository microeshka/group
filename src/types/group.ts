
export interface Group extends GroupUpdate {
  createdAt: Date;
  creatorId: string;
}

export interface GroupCreate {
  name: string;
  description: string;
  creatorId?: string;
}

export interface GroupUpdate extends GroupCreate {
  id: string;
}

export interface UserShort {
  id: string;
  fullName: string;
  roles: string[];
}

export interface Member {
  id: string;
  fullName: string;
  email: string;
  username: string;
  roles: RoleShort[];
}

export interface SubjectShort {
  id: string;
  title: string;
  description: string;
  groupId: string;
}

export interface GroupFull extends GroupCreate {
  id: string;
  subjects: SubjectShort[];
  members: Member[];
  roles: Role[];
}

export type GroupShort = GroupUpdate

export interface Permission {
  id: string;
  name: string;
}

export interface RoleShort {
  id: string;
  name: string;
  groupId: string;
}

export interface Role extends RoleShort {
  permissions: Permission[];
}

export interface RoleCreate extends RoleShort {
  permissions: string[];
}

export type RoleUpdate = RoleCreate

export interface RoleAdd {
  userId: string;
  groupId: string;
  roleId: string;
}

export interface RoleDelete {
  userId: string;
  roleId: string;
}

export interface UserSession {
  id: string;
  userId: string;
  username: string;
  fullName: string;
  validUntil: Date;
}

export interface QueueShort {
  id: string;
  title: string;
}

export interface UserBase {
  id: string;
  email: string;
  fullName: string;
  username: string;
}

export interface UserProfile extends UserBase {
  roles: string[];
  groupsInCommon: GroupShort[];
  authoredSubjects: SubjectShort[];
  authoredQueues: QueueShort[];
}
