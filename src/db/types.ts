import { Group, GroupFull, GroupShort, GroupUpdate, Member, Permission, Role, RoleCreate, RoleUpdate, UserProfile } from '../types/group';

export interface IGroupRepository {
  saveGroup: (user: Group) => Promise<void>;
  getGroupById: (id: string) => Promise<Group | undefined>;
  updateGroup: (group: GroupUpdate) => Promise<void>;
  deleteGroupById: (id: string) => Promise<void>;
  deleteUserFromGroupById: (userId: string, groupId: string) => Promise<void>;
  join: (groupId: string, userId: string) => Promise<void>;
  getGroupByJoinId: (id: string) => Promise<Group | undefined>;
  getFullGroup: (id: string) => Promise<GroupFull | undefined>;
  getAllUserGroups: (id: string) => Promise<GroupShort[]>;
  getJoinId: (id: string) => Promise<string | undefined>;
  getRolesByGroupId: (groupId: string) => Promise<Role[]>;
  getGroupMembers: (groupId: string) => Promise<Member[]>;
  saveJoinId: (joinId: string, id: string) => Promise<void>;
  getMember: (userId: string, groupId: string) => Promise<Member | undefined>;
  getUserProfile: (userId: string, profileId: string, groupId: string) => Promise<UserProfile | undefined>;
}

export interface IRoleRepository {
  getUserRoles: (userId: string, groupId: string) => Promise<string[]>;
  getUserPermissions: (userId: string, groupId: string) => Promise<Permission[]>;
  getAllPermissions: () => Promise<Permission[]>;
  saveRole: (role: RoleCreate) => Promise<Role>;
  updateRole: (role: RoleUpdate) => Promise<Role>;
  findRoleById: (roleId: string) => Promise<Role | undefined>;
  deleteRole: (roleId: string) => Promise<void>;
  addRoleToUser: (userId: string, roleId: string, groupId: string) => Promise<void>;
  deleteUserRole: (userId: string, roleId: string) => Promise<void>;
}
