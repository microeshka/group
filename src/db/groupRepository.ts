import { Pool, PoolConfig } from 'pg';
import { Group, GroupFull, GroupShort, GroupUpdate, Member, QueueShort, Role, SubjectShort, UserBase, UserProfile } from '../types/group';
import { IGroupRepository } from './types';
import * as uuid from 'uuid';

export class Postgres implements IGroupRepository {
  private connectionPool: Pool;

  constructor(opts: PoolConfig) {
    try {
      this.connectionPool = new Pool(opts);
    } catch (e) {
      console.error('Failed to connect to Postgres: ', e);
      throw e;
    }
  }

  public async updateGroup(group: GroupUpdate): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query(
        'update groups set name=$2, description=$3 where id=$1',
        [
          group.id,
          group.name,
          group.description,
        ]);
    client.release();
  }

  public async deleteGroupById(id: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query(
        'delete from groups where id = $1',
        [id],
    );
  }

  public async deleteUserFromGroupById(userId: string, groupId: string): Promise<void> {
    const client = await this.connectionPool.connect();
    const res = (await client.query('select id from groups2users where user_id=$1 and group_id = $2', [userId, groupId])).rows;
    if (res.length < 1) {
      throw { status: 404, errorMessage: 'There is no user in this group' };
    }
    await client.query(
        'delete from groups2users2roles where user_id = $1 and group_id = $2',
        [userId, groupId],
    );

    await client.query(
        'delete from groups2users where user_id = $1 and group_id = $2',
        [userId, groupId],
    );
  }

  public async getGroupById(id: string): Promise<Group | undefined> {
    const client = await this.connectionPool.connect();
    const rows = (await client.query(
        'select * from groups where id = $1',
        [id],
    )).rows;
    client.release();
    const rawGroup = rows[0];
    if (!rawGroup) {
      return rawGroup;
    }
    return {
      id: rawGroup.id,
      name: rawGroup.name,
      description: rawGroup.description,
      creatorId: rawGroup.creator_id,
      createdAt: new Date(rawGroup.created_at),
    };
  }

  public async saveGroup(group: Group): Promise<void> {
    const client = await this.connectionPool.connect();
    try {
      await client.query('BEGIN');
      await client.query(
          'insert into groups (id, name, description, creator_id, created_at) values ($1, $2, $3, $4, $5)',
          [
            group.id,
            group.name,
            group.description,
            group.creatorId,
            group.createdAt,
          ]);
      await client.query('insert into groups2users values ($1, $2)', [group.id, group.creatorId]);
      const ownerRoleId = uuid.v4();
      const memberRoleId = uuid.v4();
      await client.query('insert into roles values ($1, $2, $3)', [ownerRoleId, 'owner', group.id]);
      await client.query('insert into roles values ($1, $2, $3)', [memberRoleId, 'member', group.id]);
      await client.query('insert into roles2permissions select $1, id from permissions where name = \'perm.group.own\'', [ownerRoleId]);
      await client.query('insert into roles2permissions select $1, id from permissions where name = \'perm.subject.own\'', [ownerRoleId]);
      await client.query('insert into roles2permissions select $1, id from permissions where name = \'perm.queue.own\'', [ownerRoleId]);
      await client.query('insert into roles2permissions select $1, id from permissions where name ~ \'*.own.edit.read\'', [memberRoleId]);
      await client.query('insert into groups2users2roles values ($1, $2, $3)', [group.id, group.creatorId, ownerRoleId]);
      await client.query('COMMIT');
    } catch (err) {
      await client.query('ROLLBACK');
      throw err;
    } finally {
      client.release();
    }
  }

  public async join(groupId: string, userId: string): Promise<void> {
    const client = await this.connectionPool.connect();
    try {
      await client.query('BEGIN');
      await client.query('insert into groups2users values ($1, $2)', [groupId, userId]);
      await client.query(
          'insert into groups2users2roles values ($1, $2, (select id from roles where name = \'member\' and group_id=$1))',
          [groupId, userId]
      );
      await client.query('COMMIT');
    } catch (err) {
      await client.query('ROLLBACK');
      throw err;
    } finally {
      client.release();
    }
  }

  public async getGroupByJoinId(id: string): Promise<Group | undefined> {
    const client = await this.connectionPool.connect();
    const rows = (await client.query(
        'select * from groups where join_id = $1', [id])
    ).rows;
    client.release();
    const rawGroup = rows[0];
    return {
      id: rawGroup.id,
      name: rawGroup.name,
      description: rawGroup.description,
      creatorId: rawGroup.creator_id,
      createdAt: new Date(rawGroup.created_at),
    };
  }

  public async getFullGroup(id: string): Promise<GroupFull | undefined> {
    const client = await this.connectionPool.connect();
    const group = (await client.query(
        'select id, name, description from groups where id=$1',
        [id])
    ).rows[0] as GroupShort;
    const users = (await client.query(`
      select g2u.user_id as id, u.full_name, u.username, u.email, array_agg(r.name) as roles
      from groups2users as g2u
        left join users as u on u.id = g2u.user_id
        left join groups2users2roles g2u2r on g2u.user_id = g2u2r.user_id
        left join roles as r on r.id = g2u2r.role_id
      where g2u.group_id=$1 and r.group_id=$1
      group by (g2u.user_id, full_name, username, email)`,
    [id])
    ).rows.map((row) => ({ id: row.id, fullName: row.full_name, roles: row.roles, email: row.email, username: row.username } as Member));
    const subjects = (await client.query(
        'select id, title, description, group_id from subjects where group_id=$1',
        [id])
    ).rows.map((row) => ({
      id: row.id, title: row.title, description: row.description, groupId: row.group_id,
    })) as SubjectShort[];
    client.release();
    const roles = await this.getRolesByGroupId(id);
    return { ...group, members: users, subjects, roles } as GroupFull;
  }

  public async getAllUserGroups(id: string): Promise<GroupShort[]> {
    const client = await this.connectionPool.connect();
    const groups = (await client.query(
        'select g.id as id, g.name as name, g.description as description from groups as g left join groups2users as g2u on g2u.group_id = g.id where g2u.user_id=$1',
        [id]
    )).rows as GroupShort[];
    client.release();
    return groups;
  }

  public async getJoinId(id: string): Promise<string | undefined> {
    const client = await this.connectionPool.connect();
    const joinId = (await client.query('select join_id from groups where id=$1', [id])).rows[0]['join_id'];
    client.release();
    return joinId;
  }

  public async saveJoinId(joinId: string, id: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('update groups set join_id=$1 where id=$2', [joinId, id]);
    client.release();
  }

  public async getRolesByGroupId(groupId: string): Promise<Role[]> {
    const client = await this.connectionPool.connect();
    let roles = (await client.query(`
      select r2p.role_id as id, r.name as name, array_agg(json_build_object('id', p.id, 'name', p.name)) as permissions
      from roles2permissions as r2p
        left join roles r on r.id = r2p.role_id
        left join permissions p on r2p.permission_id = p.id
      where r.group_id=$1 and r.group_id=$1
      group by (r2p.role_id, r.name)`,
    [groupId]
    )).rows;
    roles = roles.map((row) => ({ ...row } as Role));
    client.release();
    return roles;
  }

  public async getGroupMembers(groupId: string): Promise<Member[]> {
    const client = await this.connectionPool.connect();
    const members = (await client.query(`
      select g2u.user_id as id, u.full_name, u.email, array_agg(r.name) as roles
      from groups2users as g2u
        left join users as u on u.id = g2u.user_id
        left join groups2users2roles g2u2r on g2u.user_id = g2u2r.user_id
        left join roles as r on r.id = g2u2r.role_id
      where g2u.group_id=$1 and r.group_id=$1
      group by (g2u.user_id, full_name, u.email)`,
    [groupId])
    ).rows.map((row) => ({ id: row.id, fullName: row['full_name'], email: row.email, roles: row.roles } as Member));
    client.release();
    return members;
  }

  public async getMember(userId: string, groupId: string): Promise<Member | undefined> {
    const client = await this.connectionPool.connect();
    const users = (await client.query('select id, full_name, email, username from users where id=$1', [userId])).rows;
    if (users.length < 1) {
      return undefined;
    }
    const user = users.map((user) => ({ id: user.id, email: user.email, username: user.username, fullName: user['full_name'] }))[0];
    const roles = (await client.query(`
      select r.id, r.name
      from roles as r
        left join groups2users2roles g2u2r on r.id = g2u2r.role_id
      where g2u2r.group_id=$1 and user_id=$2`,
    [groupId, userId]
    )).rows;
    if (roles.length < 1) {
      return undefined;
    }
    client.release();
    return { ...user, roles } as Member;
  }

  public async getUserProfile(userId: string, profileId: string, groupId: string): Promise<UserProfile | undefined> {
    const client = await this.connectionPool.connect();
    const userBase = (await client.query(
        'select id, email, full_name, username from users where id=$1',
        [profileId]
    )).rows.map((row) => ({
      id: row.id, email: row.email, username: row.username, fullName: row['full_name'],
    }))[0] as (UserBase | undefined);
    if (!userBase) {
      throw { status: 404, message: `User with id ${profileId} not found` };
    }
    const roles = (await client.query(
        'select name from roles where roles.id in (select role_id from groups2users2roles where user_id=$2 and group_id=$1)',
        [groupId, profileId]
    )).rows as ({ name: string }[]);
    const groupsInCommon = (await client.query(
        'select g.id as id, g.name as name from groups2users as g2u left join groups as g on g.id = g2u.group_id group by (g.id, g.name) having array_agg(g2u.user_id) @> ARRAY[$1::uuid, $2::uuid]',
        [profileId, userId]
    )).rows as GroupShort[];
    const authoredSubjects = (await client.query(
        'select id, title from subjects where creator_id=$1 and group_id=$2',
        [profileId, groupId]
    )).rows as SubjectShort[];
    const authoredQueues = (await client.query(
        'select q.id as id, q.title as title from queues as q left join subjects as s on s.id = q.subject_id where q.creator_id=$1 and s.group_id=$2',
        [profileId, groupId]
    )).rows as QueueShort[];
    client.release();
    return { ...userBase, roles: roles.map((role) => role.name), groupsInCommon, authoredSubjects, authoredQueues } as UserProfile;
  }
}
