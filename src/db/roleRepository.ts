import { Pool, PoolConfig } from 'pg';
import { Permission, RoleCreate, Role, RoleUpdate, RoleShort } from '../types/group';
import { IRoleRepository } from './types';

export class RoleRepository implements IRoleRepository {
  private connectionPool: Pool;

  constructor(opts: PoolConfig) {
    try {
      this.connectionPool = new Pool(opts);
    } catch (e) {
      console.error('Failed to connect to Postgres: ', e);
      throw e;
    }
  }

  public async deleteUserRole(userId: string, roleId: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('delete from groups2users2roles where user_id=$1 and role_id=$2', [userId, roleId]);
    client.release();
  }

  public async getUserRoles(userId: string, groupId: string): Promise<string[]> {
    const client = await this.connectionPool.connect();
    const roles = (await client.query(
      'select name from roles where roles.id in (select role_id from groups2users2roles where user_id=$2 and group_id=$1)',
      [groupId, userId]
    )).rows as ({ name: string }[]);
    client.release();
    return roles.map((role) => role.name);
  }

  public async getUserPermissions(userId: string, groupId: string): Promise<Permission[]> {
    const client = await this.connectionPool.connect();
    const permissions = (await client.query(
      `select p.id as id, p.name as name from groups2users2roles g2u2r
      left join roles2permissions r2p on r2p.role_id = g2u2r.role_id
      left join permissions p on p.id = r2p.permission_id
      where g2u2r.group_id=$1 and g2u2r.user_id =$2`,
      [groupId, userId]
    )).rows as ({ id: string, name: string }[]);
    client.release();
    return permissions;
  }

  public async getAllPermissions(): Promise<Permission[]> {
    const client = await this.connectionPool.connect();
    const permissions = (await client.query('select id, name from permissions')).rows;
    client.release();
    return permissions;
  }

  public async saveRole(role: RoleCreate): Promise<Role> {
    const client = await this.connectionPool.connect();
    try {
      await client.query('BEGIN');
      await client.query('insert into roles values ($1, $2, $3)', [role.id, role.name, role.groupId]);
      await client.query('insert into roles2permissions select $1, id from unnest($2::uuid[]) as id', [role.id, role.permissions]);
      await client.query('COMMIT');
    } catch (err) {
      await client.query('ROLLBACK');
      throw err;
    } finally {
      client.release();
    }
    return this.findRoleByIdUnsafe(role.id);
  }

  public async updateRole(role: RoleUpdate): Promise<Role> {
    const client = await this.connectionPool.connect();
    try {
      await client.query('BEGIN');
      await client.query('update roles set name=$1 where id=$2', [role.name, role.id]);
      await client.query('delete from roles2permissions where role_id=$1', [role.id]);
      await client.query('insert into roles2permissions select $1, id from unnest($2::uuid[]) as id', [role.id, role.permissions]);
      await client.query('COMMIT');
    } catch (err) {
      await client.query('ROLLBACK');
      throw err;
    } finally {
      client.release();
    }
    return this.findRoleByIdUnsafe(role.id);
  }

  public async findRoleByIdUnsafe(roleId: string): Promise<Role> {
    const client = await this.connectionPool.connect();
    const roles = (await client.query('select id, name, group_id from roles where id=$1', [roleId])).rows
      .map((role) => ({ id: role.id, name: role.name, groupId: role['group_id'] }));
    const rolePermissions = (await client.query(
      'select p.id as id, p.name as name from permissions as p left join roles2permissions r2p on p.id = r2p.permission_id where r2p.role_id=$1',
      [roleId]
    )).rows;
    client.release();
    return { id: roles[0].id, name: roles[0].name, groupId: roles[0].groupId, permissions: rolePermissions } as Role;
  }

  public async findRoleById(roleId: string): Promise<Role | undefined> {
    const client = await this.connectionPool.connect();
    const roles = (await client.query('select id from roles where id=$1', [roleId])).rows;
    if (roles.length < 1) {
      return undefined;
    }
    return this.findRoleByIdUnsafe(roleId);
  }

  public async deleteRole(roleId: string): Promise<void> {
    const client = await this.connectionPool.connect();
    try {
      const roles = (await client.query('select id, name, group_id from roles where id=$1', [roleId])).rows
        .map((role) => ({ id: role.id, name: role.name, groupId: role['group_id'] } as RoleShort));
      const role = roles[0];
      const userCount = (await client.query(
        'select count(*) as count from groups2users2roles where role_id=$1 and group_id=$2',
        [role.id, role.groupId]
      )).rows[0].count;
      console.log(`count users with role ${role.id}: ${userCount}`);
      if (userCount > 0) {
        throw { status: 400, errorMessage: 'Exist user with this role. Deletion aborted!' };
      }
      await client.query('BEGIN');
      await client.query('delete from roles2permissions where role_id=$1', [role.id]);
      await client.query('delete from roles where id=$1', [role.id]);
      await client.query('COMMIT');
    } catch (err) {
      await client.query('ROLLBACK');
      throw err;
    } finally {
      client.release();
    }
  }

  public async addRoleToUser(userId: string, roleId: string, groupId: string): Promise<void> {
    const client = await this.connectionPool.connect();
    await client.query('insert into groups2users2roles values ($1, $2, $3)', [groupId, userId, roleId]);
    client.release();
  }
}
