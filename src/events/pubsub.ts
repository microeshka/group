import { PubSub } from '@google-cloud/pubsub';
import { checkAndGetEnv } from '../utils/utils';

const pubSubClient = new PubSub({ keyFilename: checkAndGetEnv('PUBSUB_APPLICATION_CREDENTIALS') });

const topicName = 'notifications';

export interface Notification {
    type: string;
    message?: string;
    data: any;
    senderId: string;
    link?: string;
}

export const publishNotification = async (data: Notification) => {
  const dataBuffer = Buffer.from(JSON.stringify(data));
  try {
    console.log('start publising');
    const messageId = await pubSubClient.topic(topicName).publish(dataBuffer);
    console.log(`Message ${messageId} published.`);
  } catch (error) {
    console.error(`Received error while publishing: ${error.message}`);
  }
};
