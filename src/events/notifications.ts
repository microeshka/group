import { publishNotification } from './pubsub';
import { Group } from '../types/group';

export const sendJoinToGroupNotification = (group: Group, senderId: string, senderUsername: string, senderFullName: string) => {
  publishNotification({
    data: { id: senderId, fullName: senderFullName, groupId: group.id },
    senderId,
    type: 'JOIN_TO_GROUP',
    message: `User ${senderFullName} has joined to group ${group.name}`,
    link: `/group/${group.id}`,
  });
};
