import { permissionsConfig } from '../config/permissions';

export interface Interceptor {
  check<Result>(action: string, permissions: string[], fn: () => Promise<Result>): Promise<Result>;
}

const includes = (permission: string, permissions: string[]): boolean => {
  for (const p of permissions) {
    if (permission.startsWith(p)) {
      return true;
    }
  }
  return false;
};

export class PermissionChecker implements Interceptor {
  public check<Result>(action: string, permissions: string[], fn: () => Promise<Result>): Promise<Result> {
    const permission = permissionsConfig[action];
    if (!permission) {
      throw { status: 500, errorMessage: `Bad permission configuration or caller error. No permission for action: ${action}` };
    } else if (permission === '*' || includes(permission, permissions)) {
      return fn();
    } else {
      throw { status: 403, errorMessage: 'Permission denied.' };
    }
  }
}
