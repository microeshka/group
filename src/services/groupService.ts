import { IGroupRepository } from '../db/types';
import { Group, GroupCreate, GroupFull, GroupShort, GroupUpdate, Member, Role, RoleShort, UserProfile } from '../types/group';
import * as uuid from 'uuid';
import { generateRandomString } from '../join';
import { findUser } from '../grpc/grpcClient';

export interface IGroupService {
  create(data: GroupCreate): Promise<Group>;
  update(data: GroupUpdate): Promise<Group>;
  deleteUserFromGroup(groupId: string, userId: string): Promise<void>;
  getById(id: string): Promise<Group>;
  getFullById(id: string): Promise<GroupFull>;
  getMembersByGroupId(id: string): Promise<Member[]>;
  getRolesByGroupId(id: string): Promise<Role[]>;
  getGroupsByUserId(userId: string): Promise<GroupShort[]>;
  getGroupMember(groupId: string, userId: string): Promise<Member | undefined>;
  getUserProfile(groupId: string, userId: string, profileId: string): Promise<UserProfile | undefined>;
  generateJoinId(groupId: string, force: boolean): Promise<string>;
  joinUserToGroup(groupId: string, userId: string): Promise<void>;
}

export class GroupService implements IGroupService {
  private database: IGroupRepository;

  constructor(database: IGroupRepository) {
    this.database = database;
  }

  public async getById(id: string): Promise<Group> {
    const group = await this.database.getGroupById(id);
    if (!group) {
      throw { status: 404, errorMessage: `Group with id=${id} not found` };
    }
    return group;
  }

  public async deleteUserFromGroup(groupId: string, userId: string): Promise<void> {
    await this.getGroupMember(groupId, userId);
    await this.database.deleteUserFromGroupById(userId, groupId);
  }

  public async getFullById(id: string): Promise<GroupFull> {
    const group = await this.database.getFullGroup(id);
    if (!group) {
      throw { status: 404, errorMessage: `Group with id=${id} not found` };
    }
    return group;
  }

  public async getMembersByGroupId(id: string): Promise<Member[]> {
    await this.getById(id);
    return this.database.getGroupMembers(id);
  }

  public async getRolesByGroupId(id: string): Promise<Role[]> {
    await this.getById(id);
    return this.database.getRolesByGroupId(id);
  }

  public async getGroupsByUserId(id: string): Promise<GroupShort[]> {
    await findUser(id);
    return this.database.getAllUserGroups(id);
  }

  public async getGroupMember(groupId: string, userId: string): Promise<Member> {
    const member = await this.database.getMember(userId, groupId);
    if (!member) {
      throw { status: 400, errorMessage: `User=${userId} do not belongs to group=${groupId}. Maybe user or group does not exist` };
    }
    return member;
  }

  public async getUserProfile(groupId: string, userId: string, profileId: string): Promise<UserProfile | undefined> {
    await findUser(userId);
    return this.database.getUserProfile(userId, profileId, groupId);
  }

  public async generateJoinId(groupId: string, force: boolean): Promise<string> {
    await this.getById(groupId);
    if (force) {
      const joinId = generateRandomString(16);
      await this.database.saveJoinId(joinId, groupId);
      return joinId;
    } else {
      const joinId = await this.database.getJoinId(groupId);
      if (joinId) {
        return joinId;
      }
      const newJoinId = generateRandomString(16);
      await this.database.saveJoinId(newJoinId, groupId);
      return newJoinId;
    }
  }

  public async joinUserToGroup(groupId: string, userId: string): Promise<void> {
    await this.getById(groupId);
    await findUser(userId);
    await this.database.join(groupId, userId);
  }

  public async create(groupData: GroupCreate): Promise<Group> {
    if (!groupData.creatorId) {
      throw { status: 400, errorMessage: 'Group creator id must be specified' };
    }
    await findUser(groupData.creatorId);
    const group = {
      id: uuid.v4(),
      name: groupData.name,
      description: groupData.description,
      createdAt: new Date(),
      creatorId: groupData.creatorId,
    } as Group;
    await this.database.saveGroup(group);
    return group;
  }

  public async update(data: GroupUpdate): Promise<Group> {
    const group = await this.getById(data.id);
    if (data.name) {
      group.name = data.name;
    }
    if (data.description) {
      group.description = data.description;
    }
    return this.getById(data.id);
  }
}
