import { IRoleRepository } from '../db/types';
import { Permission, Role, RoleAdd, RoleCreate, RoleDelete, RoleUpdate } from '../types/group';
import * as uuid from 'uuid';
import { IGroupService } from './groupService';

export interface IRoleService {
  getAllPermissions(): Promise<Permission[]>;
  getRoleById(id: string): Promise<Role>;
  getUserRoles(groupId: string, userId: string): Promise<string[]>;
  getUserPermissions(groupId: string, userId: string): Promise<Permission[]>;
  createRole(data: RoleCreate): Promise<Role>;
  updateRole(data: RoleUpdate): Promise<Role>;
  deleteRole(id: string): Promise<void>;
  assignRole(data: RoleAdd): Promise<void>;
  unassignRole(data: RoleDelete): Promise<void>;
}

export class RoleService implements IRoleService {
  private roleRepository: IRoleRepository;
  private groupService: IGroupService;

  constructor(roleRepository: IRoleRepository, groupService: IGroupService) {
    this.roleRepository = roleRepository;
    this.groupService = groupService;
  }

  public async getAllPermissions(): Promise<Permission[]> {
    return this.roleRepository.getAllPermissions();
  }

  public async getRoleById(id: string): Promise<Role> {
    const role = await this.roleRepository.findRoleById(id);
    if (!role) {
      throw { status: 404, errorMessage: `Role with id=${id} not found` };
    }
    return role;
  }

  public async getUserRoles(groupId: string, userId: string): Promise<string[]> {
    await this.groupService.getGroupMember(groupId, userId);
    return this.roleRepository.getUserRoles(userId, groupId);
  }

  public async getUserPermissions(groupId: string, userId: string): Promise<Permission[]> {
    await this.groupService.getGroupMember(groupId, userId);
    return this.roleRepository.getUserPermissions(userId, groupId);
  }

  public async createRole(data: RoleCreate): Promise<Role> {
    await this.groupService.getById(data.groupId);
    if (data.permissions.length < 1) {
      throw { status: 400, errorMessage: 'Role can not contain less the one permission!' };
    }
    data.id = uuid.v4();
    return this.roleRepository.saveRole(data);
  }

  public async updateRole(data: RoleCreate): Promise<Role> {
    await this.getRoleById(data.id);
    if (!data.name) {
      throw { status: 400, errorMessage: `Role name must be defined. Got: ${data.name}` };
    }
    if (data.permissions.length < 1) {
      throw { status: 400, errorMessage: 'Role can not contain less the one permission!' };
    }
    return this.roleRepository.updateRole(data);
  }

  public async deleteRole(id: string): Promise<void> {
    await this.getRoleById(id);
    return this.roleRepository.deleteRole(id);
  }

  public async assignRole(data: RoleAdd): Promise<void> {
    const role = await this.getRoleById(data.roleId);
    const roles = await this.getUserRoles(role.groupId, data.userId);
    for (const r of roles) {
      if (r === role.name) {
        throw { status: 400, errorMessage: 'User already have this role!' };
      }
    }
    return this.roleRepository.addRoleToUser(data.userId, data.roleId, data.groupId);
  }

  public async unassignRole(data: RoleDelete): Promise<void> {
    const role = await this.getRoleById(data.roleId);
    const roles = await this.getUserRoles(role.groupId, data.userId);
    for (const r of roles) {
      if (r === role.name) {
        return this.roleRepository.deleteUserRole(data.userId, data.roleId);
      }
    }
    throw { status: 400, errorMessage: 'User already do not have this role!' };
  }
}
