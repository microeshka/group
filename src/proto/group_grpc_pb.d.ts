// package: group
// file: group.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as group_pb from "./group_pb";

interface IGroupService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    getUserPermissions: IGroupService_IgetUserPermissions;
}

interface IGroupService_IgetUserPermissions extends grpc.MethodDefinition<group_pb.PermissionsRequest, group_pb.PermissionsResponse> {
    path: "/group.Group/getUserPermissions";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<group_pb.PermissionsRequest>;
    requestDeserialize: grpc.deserialize<group_pb.PermissionsRequest>;
    responseSerialize: grpc.serialize<group_pb.PermissionsResponse>;
    responseDeserialize: grpc.deserialize<group_pb.PermissionsResponse>;
}

export const GroupService: IGroupService;

export interface IGroupServer {
    getUserPermissions: grpc.handleUnaryCall<group_pb.PermissionsRequest, group_pb.PermissionsResponse>;
}

export interface IGroupClient {
    getUserPermissions(request: group_pb.PermissionsRequest, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
    getUserPermissions(request: group_pb.PermissionsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
    getUserPermissions(request: group_pb.PermissionsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
}

export class GroupClient extends grpc.Client implements IGroupClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public getUserPermissions(request: group_pb.PermissionsRequest, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
    public getUserPermissions(request: group_pb.PermissionsRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
    public getUserPermissions(request: group_pb.PermissionsRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: group_pb.PermissionsResponse) => void): grpc.ClientUnaryCall;
}
