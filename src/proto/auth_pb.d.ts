// package: auth
// file: auth.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class FindSessionRequest extends jspb.Message { 
    getId(): string;
    setId(value: string): FindSessionRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FindSessionRequest.AsObject;
    static toObject(includeInstance: boolean, msg: FindSessionRequest): FindSessionRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FindSessionRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FindSessionRequest;
    static deserializeBinaryFromReader(message: FindSessionRequest, reader: jspb.BinaryReader): FindSessionRequest;
}

export namespace FindSessionRequest {
    export type AsObject = {
        id: string,
    }
}

export class FindUserRequest extends jspb.Message { 
    getId(): string;
    setId(value: string): FindUserRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): FindUserRequest.AsObject;
    static toObject(includeInstance: boolean, msg: FindUserRequest): FindUserRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: FindUserRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): FindUserRequest;
    static deserializeBinaryFromReader(message: FindUserRequest, reader: jspb.BinaryReader): FindUserRequest;
}

export namespace FindUserRequest {
    export type AsObject = {
        id: string,
    }
}

export class User extends jspb.Message { 
    getId(): string;
    setId(value: string): User;
    getEmail(): string;
    setEmail(value: string): User;
    getFullName(): string;
    setFullName(value: string): User;
    getUsername(): string;
    setUsername(value: string): User;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): User.AsObject;
    static toObject(includeInstance: boolean, msg: User): User.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): User;
    static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
    export type AsObject = {
        id: string,
        email: string,
        fullName: string,
        username: string,
    }
}

export class Session extends jspb.Message { 
    getId(): string;
    setId(value: string): Session;
    getUserId(): string;
    setUserId(value: string): Session;
    getUsername(): string;
    setUsername(value: string): Session;
    getFullName(): string;
    setFullName(value: string): Session;
    getValidUntil(): number;
    setValidUntil(value: number): Session;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Session.AsObject;
    static toObject(includeInstance: boolean, msg: Session): Session.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Session, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Session;
    static deserializeBinaryFromReader(message: Session, reader: jspb.BinaryReader): Session;
}

export namespace Session {
    export type AsObject = {
        id: string,
        userId: string,
        username: string,
        fullName: string,
        validUntil: number,
    }
}
