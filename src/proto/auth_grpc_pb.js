// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var auth_pb = require('./auth_pb.js');

function serialize_auth_FindSessionRequest(arg) {
  if (!(arg instanceof auth_pb.FindSessionRequest)) {
    throw new Error('Expected argument of type auth.FindSessionRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_FindSessionRequest(buffer_arg) {
  return auth_pb.FindSessionRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_FindUserRequest(arg) {
  if (!(arg instanceof auth_pb.FindUserRequest)) {
    throw new Error('Expected argument of type auth.FindUserRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_FindUserRequest(buffer_arg) {
  return auth_pb.FindUserRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_Session(arg) {
  if (!(arg instanceof auth_pb.Session)) {
    throw new Error('Expected argument of type auth.Session');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_Session(buffer_arg) {
  return auth_pb.Session.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_auth_User(arg) {
  if (!(arg instanceof auth_pb.User)) {
    throw new Error('Expected argument of type auth.User');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_auth_User(buffer_arg) {
  return auth_pb.User.deserializeBinary(new Uint8Array(buffer_arg));
}


var AuthService = exports.AuthService = {
  findSession: {
    path: '/auth.Auth/findSession',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.FindSessionRequest,
    responseType: auth_pb.Session,
    requestSerialize: serialize_auth_FindSessionRequest,
    requestDeserialize: deserialize_auth_FindSessionRequest,
    responseSerialize: serialize_auth_Session,
    responseDeserialize: deserialize_auth_Session,
  },
  findUser: {
    path: '/auth.Auth/findUser',
    requestStream: false,
    responseStream: false,
    requestType: auth_pb.FindUserRequest,
    responseType: auth_pb.User,
    requestSerialize: serialize_auth_FindUserRequest,
    requestDeserialize: deserialize_auth_FindUserRequest,
    responseSerialize: serialize_auth_User,
    responseDeserialize: deserialize_auth_User,
  },
};

exports.AuthClient = grpc.makeGenericClientConstructor(AuthService);
