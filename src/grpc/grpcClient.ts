import { AuthClient } from '../proto/auth_grpc_pb';
import * as grpc from 'grpc';
import { FindSessionRequest, FindUserRequest, Session, User } from '../proto/auth_pb';
import { GrpcCallError } from '../utils/errors';
import { UserBase, UserSession } from '../types/group';
import { checkAndGetEnv } from '../utils/utils';

const client = new AuthClient(checkAndGetEnv('GRPC_AUTH_CLIENT'), grpc.credentials.createInsecure());

const protoSessionToUserSession = (data: Session.AsObject): UserSession => ({
  ...data,
  validUntil: new Date(data.validUntil),
});

const protoUserToUser = (data: User.AsObject): UserBase => ({
  ...data
});

export const findSession = async (id: string): Promise<UserSession> => {
  const request = new FindSessionRequest();
  request.setId(id);
  return new Promise((resolve, reject) => client.findSession(
    request,
    (err, resp) => err ? reject(new GrpcCallError(err)) : resolve(protoSessionToUserSession(resp.toObject()))
  ));
};

export const findUser = async (userId: string): Promise<UserBase> => {
  const request = new FindUserRequest();
  request.setId(userId);
  return new Promise((resolve, reject) => client.findUser(
    request,
    (err, resp) => err ? reject(new GrpcCallError(err)) : resolve(protoUserToUser(resp.toObject()))
  ));
};

export const findSessionExtended = async (id: string, connectionAddr: string): Promise<UserSession> => {
  const client = new AuthClient(connectionAddr, grpc.credentials.createInsecure());
  const request = new FindSessionRequest();
  request.setId(id);
  return new Promise((resolve, reject) => client.findSession(
    request,
    (err, resp) => err ? reject(new GrpcCallError(err)) : resolve(protoSessionToUserSession(resp.toObject()))
  ));
};
