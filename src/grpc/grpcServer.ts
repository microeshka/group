import * as grpc from 'grpc';
import { IGroupServer } from '../proto/group_grpc_pb';
import { PermissionsRequest, Permission, PermissionsResponse } from '../proto/group_pb';
import { IRoleService } from '../services/roleService';
import { errorToGrpcError } from '../utils/utils';

export class GroupServer implements IGroupServer {
  private roleService: IRoleService;

  constructor(roleService: IRoleService) {
    this.roleService = roleService;
  }

  async getUserPermissions(call: grpc.ServerUnaryCall<PermissionsRequest>, callback: grpc.sendUnaryData<PermissionsResponse>): Promise<void> {
    try {
      const permissions = (await this.roleService.getUserPermissions(
          call.request.getGroupId(),
          call.request.getUserId()
      )).map((p) => {
        const resPerm = new Permission();
        resPerm.setId(p.id);
        resPerm.setName(p.name);
        return resPerm;
      });
      const response = new PermissionsResponse();
      response.setPermissionsList(permissions);
      callback(null, response);
    } catch (err) {
      console.error(err);
      callback(errorToGrpcError(err), null);
    }
  }
}
