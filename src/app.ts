import { Postgres } from './db/groupRepository';
import { GroupService, IGroupService } from './services/groupService';
import { IRoleService, RoleService } from './services/roleService';
import { RoleRepository } from './db/roleRepository';
import { createFastifyServer } from './config/fastifyServer';
import { PermissionChecker } from './interceptor';
import * as grpc from 'grpc';
import { IGroupServer as IGrpcGroupServer, GroupService as GrpcGroupService } from './proto/group_grpc_pb';
import { GroupServer as GrpcGroupServer } from './grpc/grpcServer';
import { checkAndGetEnv } from './utils/utils';
import { PoolConfig } from 'pg';

export class Application {
  private groupService: IGroupService;
  private roleService: IRoleService;
  private fastifyServer: any;
  private grpcServer: grpc.Server;

  constructor() {
    const dbConfig = {
      max: 20,
      idleTimeoutMillis: 10000,
      connectionTimeoutMillis: 2000,
      connectionString: checkAndGetEnv('MAIN_DB_CONNECTION_STRING'),
    } as PoolConfig;
    const groupRepository = new Postgres(dbConfig);
    const roleRepository = new RoleRepository(dbConfig);
    const interceptor = new PermissionChecker();
    this.groupService = new GroupService(groupRepository);
    this.roleService = new RoleService(roleRepository, this.groupService);

    this.fastifyServer = createFastifyServer(this.groupService, this.roleService, interceptor);

    this.grpcServer = new grpc.Server();
    this.grpcServer.addService<IGrpcGroupServer>(GrpcGroupService, new GrpcGroupServer(this.roleService));
    this.grpcServer.bind(`0.0.0.0:${checkAndGetEnv('GRPC_SERVER_PORT')}`, grpc.ServerCredentials.createInsecure());
  }

  public startFastifyServer() {
    const port = checkAndGetEnv('HTTP_SERVER_PORT');
    this.fastifyServer.listen({ port }).then(() =>
      console.log(`App started! PORT: ${port}`),
    );
  }

  public startGrpcServer() {
    this.grpcServer.start();
    console.log(`Grpc server started on port ${checkAndGetEnv('GRPC_SERVER_PORT')}`);
  }
}
