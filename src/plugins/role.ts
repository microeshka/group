import { FastifyPluginCallback, RouteShorthandOptions } from 'fastify';
import { findSession } from '../grpc/grpcClient';
import { Interceptor } from '../interceptor';
import { IRoleService } from '../services/roleService';
import { RoleAdd, RoleCreate, RoleDelete, RoleUpdate } from '../types/group';

export interface RolePluginOptions {
  roleService: IRoleService;
  interceptor: Interceptor;
}

const createRoleSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['name', 'groupId', 'permissions'],
      properties: {
        name: { type: 'string' },
        groupId: { type: 'string' },
        permissions: { type: 'array' },
      },
    },
  },
};

const updateRoleSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['id', 'name', 'groupId', 'permissions'],
      properties: {
        id: { type: 'string' },
        name: { type: 'string' },
        groupId: { type: 'string' },
        permissions: { type: 'array' },
      },
    },
  },
};

const assignRoleSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['groupId', 'userId', 'roleId'],
      properties: {
        groupId: { type: 'string' },
        userId: { type: 'string' },
        roleId: { type: 'string' },
      },
    },
  },
};

const unassignRoleSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['userId', 'roleId'],
      properties: {
        userId: { type: 'string' },
        roleId: { type: 'string' },
      },
    },
  },
};

export const rolePlugin: FastifyPluginCallback<RolePluginOptions> = (fastify, options, done) => {
  fastify.get('/group/permissions', async (request, response) => {
    try {
      response.code(200).send(JSON.stringify(await options.roleService.getAllPermissions()));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  fastify.get('/group/:groupId/role/:id', async (request, response) => {
    const { id, groupId } = request.params as { id: string, groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readRole', permissions, () => options.roleService.getRoleById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:groupId/member/:userId/role', async (request, response) => {
    const { groupId, userId } = request.params as { groupId: string, userId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readRole', permissions, () => options.roleService.getUserRoles(groupId, userId)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.post('/group/role', createRoleSchema, async (request, response) => {
    const roleCreate = request.body as RoleCreate;
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(roleCreate.groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'createRole', permissions, () => options.roleService.createRole(roleCreate)))
      );
      response.code(201).send(JSON.stringify(await options.roleService.createRole(request.body as RoleCreate)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  fastify.put('/group/role', updateRoleSchema, async (request, response) => {
    const roleUpdate = request.body as RoleUpdate;
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(roleUpdate.groupId, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'updateRole', permissions, () => options.roleService.updateRole(roleUpdate)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  fastify.delete('/group/:groupId/role/:id', async (request, response) => {
    const { id, groupId } = request.params as { id: string, groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'deleteRole', permissions, () => options.roleService.deleteRole(id)
      );
      response.code(204);
    } catch (err) {
      console.log(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  fastify.post('/group/role/add', assignRoleSchema, async (request, response) => {
    const roleAdd = request.body as RoleAdd;
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(roleAdd.groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'assignRole', permissions, () => options.roleService.assignRole(roleAdd)
      );
      response.code(200).send('');
    } catch (err) {
      console.log(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  fastify.delete('/group/:groupId/role', unassignRoleSchema, async (request, response) => {
    const roleDelete = request.body as RoleDelete;
    const { groupId } = request.params as { groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'unassignRole', permissions, () => options.roleService.unassignRole(roleDelete)
      );
      response.code(204);
    } catch (err) {
      console.log(err);
      response.code(err.status || 500).send(err.message || err);
    }
  });

  done();
};
