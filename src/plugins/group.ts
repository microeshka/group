import { FastifyPluginCallback, RouteShorthandOptions } from 'fastify';
import { findSession, findSessionExtended } from '../grpc/grpcClient';
import { Interceptor } from '../interceptor';
import { IGroupService } from '../services/groupService';
import { IRoleService } from '../services/roleService';
import { GroupCreate, GroupUpdate } from '../types/group';

export interface GroupPluginOptions {
  groupService: IGroupService;
  roleService: IRoleService;
  interceptor: Interceptor;
}

const createSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['name', 'description'],
      properties: {
        name: { type: 'string' },
        description: { type: 'string' },
      },
    },
  },
};

const updateSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['id', 'name', 'description'],
      properties: {
        id: { type: 'string' },
        name: { type: 'string' },
        description: { type: 'string' },
      },
    },
  },
};

const generateJoinIdSchema: RouteShorthandOptions = {
  schema: {
    body: {
      type: 'object',
      required: ['force'],
      properties: {
        force: { type: 'boolean' },
      },
    },
  },
};

export const groupPlugin: FastifyPluginCallback<GroupPluginOptions> = (fastify, options, done) => {
  fastify.post('/group', createSchema, async (request, response) => {
    const groupData = request.body as GroupCreate;
    try {
      const session = await findSession(request.cookies['Session']);
      groupData.creatorId = session.userId;
      response.code(200).send(JSON.stringify(await options.groupService.create(groupData)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.delete('/group/kick-out/:groupId/:userId', async (request, response) => {
    const { userId, groupId } = request.params as { userId: string, groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupId, session.userId)).map((p) => p.name);
      await options.interceptor.check(
          'kickMember', permissions, () => options.groupService.deleteUserFromGroup(groupId, userId)
      );
      response.code(204);
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.put('/group', updateSchema, async (request, response) => {
    const groupData = request.body as GroupUpdate;
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(groupData.id, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'updateGroup', permissions, () => options.groupService.update(groupData)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:id', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(id, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readGroup', permissions, () => options.groupService.getById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:id/full', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(id, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readGroup', permissions, () => options.groupService.getFullById(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:id/member', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(id, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readGroup', permissions, () => options.groupService.getMembersByGroupId(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err?.status || 500).send(err);
    }
  });

  fastify.get('/group/:id/roles', async (request, response) => {
    const { id } = request.params as { id: string };
    try {
      const session = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(id, session.userId)).map((p) => p.name);
      response.code(200).send(JSON.stringify(await options.interceptor.check(
          'readRole', permissions, () => options.groupService.getRolesByGroupId(id)))
      );
    } catch (err) {
      console.error(err);
      response.code(err?.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/all', async (request, response) => {
    try {
      const session = await findSession(request.cookies['Session']);
      response.code(200).send(JSON.stringify(await options.groupService.getGroupsByUserId(session.userId)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:groupId/member/:profileId', async (request, response) => {
    const { profileId, groupId } = request.params as { profileId: string, groupId: string };
    try {
      response.code(200).send(JSON.stringify(await options.groupService.getGroupMember(groupId, profileId)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.get('/group/:groupId/profile/:profileId', async (request, response) => {
    const { profileId, groupId } = request.params as { profileId: string, groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      response.code(200).send(JSON.stringify(await options.groupService.getUserProfile(groupId, session.userId, profileId)));
    } catch (err) {
      console.error(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.post('/group/:groupId/join', async (request, response) => {
    const { groupId } = request.params as { groupId: string };
    try {
      const session = await findSession(request.cookies['Session']);
      await options.groupService.joinUserToGroup(groupId, session.userId);
      response.code(200).send('Join success!');
    } catch (err) {
      console.log(err);
      response.code(err.status || 500).send(err.errorMessage || err);
    }
  });

  fastify.post('/group/join-id/:id', generateJoinIdSchema, async (request, response) => {
    const { id } = request.params as { id: string };
    const { force } = request.body as { force: boolean };
    try {
      const { userId } = await findSession(request.cookies['Session']);
      const permissions = (await options.roleService.getUserPermissions(id, userId)).map((p) => p.name);
      const inviteId = await options.interceptor.check(
          'generateJoinId', permissions, () => options.groupService.generateJoinId(id, force)
      );
      response.code(200).send(inviteId);
    } catch (err) {
      console.log(err);
      response.code(err.status || 400).send(err.errorMessage || err);
    }
  });

  done();
};
