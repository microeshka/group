import { FastifyPluginCallback } from 'fastify';

export const healthCheckPlugin: FastifyPluginCallback<Record<string, never>> = (fastify, options, done) => {
  fastify.get('/group/health', (request, response) => {
    // console.log('g1: health check');
    response.code(200).send('ok');
  });

  fastify.get('/healthz', (request, response) => {
    // console.log('g2: health check');
    response.code(200).send('ok');
  });

  fastify.get('/', (request, response) => {
    // console.log('g3: health check');
    response.code(200).send('ok');
  });

  done();
};
