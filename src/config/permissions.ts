
export const permissionsConfig = {
  'createGroup': '*', // anyone can perform this action
  'updateGroup': 'perm.group.own.edit',
  'kickMember': 'perm.group.own.members.kick',
  'readGroup': 'perm.group.own.edit.read',
  'generateJoinId': 'perm.group.own.members.invite',
  'readRole': 'perm.group.own.edit.read',
  'createRole': 'perm.group.own.members.editperm',
  'updateRole': 'perm.group.own.members.editperm',
  'deleteRole': 'perm.group.own.members.editperm',
  'assignRole': 'perm.group.own.members.editperm',
  'unassignRole': 'perm.group.own.members.editperm',
} as { [key: string]: string };
