import { groupPlugin, GroupPluginOptions } from '../plugins/group';
import { rolePlugin, RolePluginOptions } from '../plugins/role';
import fastify, { FastifyReply, FastifyRequest, HookHandlerDoneFunction } from 'fastify';
import cors from 'fastify-cors';
import { healthCheckPlugin } from '../plugins/healthCheck';
import fastifyCookie from 'fastify-cookie';
import { IGroupService } from '../services/groupService';
import { IRoleService } from '../services/roleService';
import { RouteGenericInterface } from 'fastify/types/route';
import { IncomingMessage, ServerResponse } from 'http';
import { Server } from 'http';
import { Interceptor } from '../interceptor';

const allowUnauthorized = new Set(['/', '/healthz', '/group/health']);

const cookieHook = (
    request: FastifyRequest<RouteGenericInterface, Server, IncomingMessage>,
    reply: FastifyReply<Server, IncomingMessage, ServerResponse, RouteGenericInterface, unknown>,
    done: HookHandlerDoneFunction
) => {
  if (!allowUnauthorized.has(request.url) && !request.cookies['Session']) {
    reply.code(401).send('User is not authorized');
  } else {
    done();
  }
};

export const createFastifyServer = (groupService: IGroupService, roleService: IRoleService, interceptor: Interceptor) => {
  const server = fastify({});
  server.register(cors, {
    origin: (_origin, cb) => {
      cb(null, true);
    },
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    credentials: true,
  });
  server.register(fastifyCookie);
  server.register(healthCheckPlugin, {});
  server.register(groupPlugin, { groupService, interceptor, roleService } as GroupPluginOptions);
  server.register(rolePlugin, { roleService, interceptor } as RolePluginOptions);
  server.addHook('onRequest', cookieHook);
  return server;
};
